# Unity SteamVR Networking Framework

This repository contains a simple framework for networking a number of users together in a VR environment in Unity. The networking solution is implemented using Photon.

The framework uses SteamVR and specifically Valve's VR Interaction System, but can be repurposed for other VR solutions.

## Features

*  A basic user avatar based on input from Valve's VR Interaction System
*  Synchronization of complex avatars with N amount of transforms
*  Interpolation of avatar transforms, allowing for smooth avatar movement with low send rates
*  Basic networked object interaction built on top of the Interactiona System's Throwable component, allowing users to pick up, move and throw objects
*  A sample Unity project with a scene demonstrating all the above

## Requirements

The framework requires the [SteamVR Plugin](https://assetstore.unity.com/packages/tools/integration/steamvr-plugin-32647) and [Photon Unity Networking 2](https://assetstore.unity.com/packages/tools/network/pun-2-free-119922) (PUN2) assets, both available for free on the Unity asset store. It was developed using PUN2 version 2.15 and SteamVR Plugin version 2.3.2.

The demo Unity project was developed using Unity 2018.3.13f1.

## Setup

The full codebase and a sample Unity project is available in this repository. We also provide a Unity package containing the framework assets. Setup is a matter of downloading either one of those and having a look at the Player Avatar prefab and/or sample scene to see how the assets work.

Regardless of your download option, you need to add the SteamVR Plugin and PUN2 Unity assets to your project as we cannot redistribute those (see [Requirements](https://gitlab.com/jonasschjerlund/unity-vr-networking-framework/blob/master/README.md#requirements)).